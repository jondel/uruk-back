module.exports = (sequelize, Sequelize, DataTypes) => {
  const Transaction = sequelize.define("transactions", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false
    },
    source: {
      type: Sequelize.STRING,
      allowNull: true
    },
    fee: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true
    },
    timestamp: {
      type: Sequelize.INTEGER
    },
    events: {
      type: Sequelize.JSON,
      allowNull: true
    },
    tokenTransfers: {
      type: Sequelize.JSON
    },
    nativeTransfers: {
      type: Sequelize.JSON,
      allowNull: true
    },
    accountData: {
      type: Sequelize.JSON,
      allowNull: true
    },
    signature: {
      type: Sequelize.STRING,
      allowNull: true
    },
    wallet: {
      type: Sequelize.STRING,
      allowNull: true
    }
  });
  return Transaction;
};