module.exports = (sequelize, Sequelize) => {
    const Pdf = sequelize.define("pdfs", {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      },
      url: {
        type: Sequelize.STRING
      }
    });
    return Pdf;
  };
  