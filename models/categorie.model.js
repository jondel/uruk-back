module.exports = (sequelize, Sequelize) => {
    const Categorie = sequelize.define("categories", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING
      }
    });
    return Categorie;
  };