module.exports = (sequelize,Sequelize) => {
    const Image = sequelize.define("images", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      url: {
        type: Sequelize.STRING
      }
     
    },{tableName : 'images'});
  
    return Image;
  };