module.exports = (sequelize, Sequelize) => {
    const Payment = sequelize.define("payments", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.INTEGER
      }
    });
    return Payment;
  };