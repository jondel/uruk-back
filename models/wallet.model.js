module.exports = (sequelize, Sequelize) => {
    const Wallet = sequelize.define("wallets", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      nativeBalance: {
        type: Sequelize.FLOAT,
        allowNull: true,
      }
    });
    return Wallet;
  };
  