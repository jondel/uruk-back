const axios = require('axios');

exports.getTokenData = async (walletData) => {

    let solData = {
        nativeBalance: 0,
        solData: [],
        tokens: [],
    };

    try {
        const tokens = await Promise.all(walletData.map(async (token) => {
            let tokenData = {};
            const amount = parseInt(token.amount)
            if (amount > 1) {
                try {
                    const { data } = await axios.get(
                        "https://public-api.solscan.io/token/meta?tokenAddress=" + token.mint, {
                        headers: {
                            "Accept": "application/json",
                            "token": `${process.env.SOLSCAN_API_KEY}`
                        }
                    }
                    );

                    if (data != undefined && data.name != "" && data.type != "nft") {
                        if (data.symbol === "SOL") {
                            solData.nativeBalance = token.amount;
                        }
                        tokenData.name = data.name;
                        tokenData.decimals = data.decimals;
                        tokenData.icon = data.icon;
                        tokenData.price = data.price;
                        tokenData.symbol = data.symbol;
                        tokenData.type = data.type;
                        tokenData.twitter = data.twitter
                        tokenData.amount = token.amount;
                        tokenData.mint = token.mint

                        return tokenData;
                    }
                } catch(err) {
                    console.log(err)
                    if (err.message) console.log(err)
                }
                
            }
        }))

        const sol = await axios.get(
            "https://public-api.solscan.io/token/meta?tokenAddress=So11111111111111111111111111111111111111112", {
            headers: {
                "Accept": "application/json",
                "token": `${process.env.SOLSCAN_API_KEY}`
            }
        }
        );

        solData.solData.push(sol.data);

        tokens.forEach(token => {
            if (token != undefined) {
                solData.tokens.push(token);
            }
        })
    } catch (err) {
        console.log(err)
        if (err.message) console.log(err)
    }

    return solData;
}

exports.getNftMetadata = async (walletData) => {

    let nfts = [];
    try {
        const nftMetadata = await Promise.all(walletData.map(async (token) => {
            const amount = parseInt(token.amount);

            if (amount === 1) {
                let nft = {};
                const optionsMints = {
                    method: 'POST',
                    url: 'https://rest-api.hellomoon.io/v0/nft/mints-by-owner',
                    headers: {
                        accept: 'application/json',
                        'content-type': 'application/json',
                        authorization: `Bearer ${process.env.HELLOMOON_API_KEY}`
                    },
                    data: { nftMint: `${token.mint}` }
                };
                const { data } = await axios.request(optionsMints);

                if (data.data.length >= 1) {
                    if (data.data[0].metadataJson.uri != '' || data.data[0].metadataJson.uri) {
                        try {
                            const response = await axios.get(data.data[0].metadataJson.uri);
                            if (response.status === 200) {
                                nft = {
                                    helloMoonMetaData: data.data[0],
                                    metadataUri: response.data
                                }

                                return nft;
                            }
                        } catch (err) {
                            if (err.status === 429) console.log("ERROR 429 !!")
                        }

                    }
                }
            }
        }))

        nftMetadata.forEach(nft => {
            if (nft) {
                nfts.push(nft);
            }
        })

    } catch (err) {
        if (err.message) console.log("ERROR !!", err.message)
    }

    return nfts;
}

exports.updateNativeBalance = async (walletAddress, daoId, wallet) => {
    try {
        const url = `https://api.helius.xyz/v0/addresses/${walletAddress}/balances?api-key=${process.env.HELIUS_API_KEY}`;

        const { data } = await axios.get(url);

        let newWallet = await wallet.update({ nativeBalance: data.nativeBalance / LAMPORT }, {
            where: {
                daoId: daoId
            }
        })
        return newWallet;

    } catch(err) {
        console.log(err.message);
        return err.message;
    }
}