const db = require("../models");
const Collection = db.collection;
const axios = require('axios');

exports.compareAndUpdateCollection = async (helloMoonCollectionId, collectionInDb) => {

  try {
    const collectionObject = await getCollectionData(helloMoonCollectionId);

    collectionInDb.update(collectionObject);

    return collectionInDb;

  } catch (err) {
    if (err.message) console.log(err.message);
  }

}

exports.getCollectionAndCreateInDb = async (helloMoonCollectionId) => {

  try {

    const collectionObject = await getCollectionData(helloMoonCollectionId);

    // create collection and feed it
    const collection = await Collection.create(collectionObject);

    return collection;

  } catch (err) {
    console.log(err);
    if (err.message) console.log(err.message)
  }
}

//---------------------------------------------------------------------------
//-------------- PRIVATE FUNCTION -------------------------------------------
//---------------------------------------------------------------------------

async function getCollectionData(helloMoonCollectionId) {
  const url_1 = {
    method: 'POST',
    url: 'https://rest-api.hellomoon.io/v0/nft/collection/leaderboard/stats',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      authorization: `Bearer ${process.env.HELLOMOON_API_KEY}`
    },
    data: { helloMoonCollectionId: helloMoonCollectionId }
  };
  const url_2 = {
    method: 'POST',
    url: 'https://rest-api.hellomoon.io/v0/nft/collection/all-time',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      authorization: `Bearer ${process.env.HELLOMOON_API_KEY}`
    },
    data: { helloMoonCollectionId: helloMoonCollectionId }
  };

  try {
    const { data } = await axios.request(url_1);

    const response = await axios.request(url_2);

    const collectionObject = {
      name: data.data[0]?.collectionName,
      floorPrice: data.data[0]?.floorPrice,
      totalSupply: parseInt(data.data[0]?.supply),
      helloMoonCollectionId: data.data[0]?.helloMoonCollectionId,
      market_cap_sol: parseFloat(data.data[0]?.market_cap_sol),
      market_cap_usd: parseFloat(data.data[0]?.market_cap_usd),
      total_volume: response.data.totalVolumeLamports,
      ath: response.data.allTimeHighPriceLamports,
      atl: response.data.allTimeLowPriceLamports,
    }

    return collectionObject;

  } catch (err) {
    console.log(err);
    if (err.message) console.log(err.message)
  }

}