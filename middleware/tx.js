isUser = (req, res, next) => {
    if(!req.body.userId) {
        res.status(403).send({
            message : "Need a user for making transactions request"
        })
        return;
    }
    next();
}

const tx = {
    isUser: isUser
};

module.exports = tx;