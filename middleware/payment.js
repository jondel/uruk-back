const db = require("../models");
const Categorie = db.categories;
const Dao = db.dao;
const Payment = db.payments;

hasCategorie = (req, res, next) => {
    Dao.findByPk(req.params.id || req.body.daoId).then(dao => {
        let categories = [];
        dao.getCategories().then(cat => {
            if(!cat) {
                return res.status(404).send({
                    message: "You cannot create a payment without categories. Please, create a categorie before."
                })
            }
            for (let i = 0; i < cat.length; i++) {
                categories.push(cat[i].name);
              }
            req.body.categories = categories;
            next();
        })
    })
    .catch(err => {
        res.status(400).send({
            "message" : err.message || "Cannot find Dao"
        })
    })
}

const payment = {
    hasCategorie: hasCategorie
}

module.exports = payment;