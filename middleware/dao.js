const { collection } = require("../models");
const db = require("../models");
const Dao = db.dao;
const User = db.user;
const Collection = db.collection;

isDaoOwner = (req, res, next) => {
    Dao.findByPk(req.query.daoId || req.params.id || req.body.daoId).then(dao => {
        dao.getUsers().then(users => {
            if(!users) {
                return res.status(404).send({
                    message: "We cannot find User for this DAO"
                })
            }
            for(let i = 0; i < users.length; i++) {
                if(users[i].id === req.body.userId) {
                    next();
                    return;
                }
                if(users[i].id === req.params.userId) {
                    next();
                    return;
                }
                if(users[i].id === req.query.userId) {
                    next();
                    return;
                }
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Require ownership of the DAO"
            })
        });
    });
};

hasDaoOwnerRole = (req, res, next) => {
    User.findByPk(req.query.userId || req.params.userId || req.body.userId).then(user => {
        user.getRoles().then(roles => {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i].name === "daoOwner") {
                  next();
                  return;
                }
              }
              res.status(403).send({
                message: "Require DaoOwner Role!"
              });
        });
    });
}

verifCollection = (req, res, next) => {
    const collection = Collection.findOne({where : {
        daoId: req.query.daoId
      }})
    
      if(collection) {
        res.status(403).send('Dao has already a collection');
      }
      next();
}

const dao = {
    isDaoOwner: isDaoOwner,
    hasDaoOwnerRole: hasDaoOwnerRole
};

module.exports = dao;