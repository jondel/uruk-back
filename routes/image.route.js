const { authJwt, dao, upload } = require("../middleware");
const controller = require("../controllers/upload.controller.js");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    "/api/image/upload",
    [
      //authJwt.verifyToken,
      //dao.hasDaoOwnerRole,
      //upload.single("fileInputDao")
    ],
    controller.uploadFiles
  );
};