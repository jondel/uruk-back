module.exports = app => {
    const { authJwt } = require("../middleware");
    const users = require("../controllers/user.controller.js");
    var router = require("express").Router();

    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
    // Retrieve all Users
    router.get("/", users.findAll);
    // Retrieve a single Tutorial with id
    router.get("/:id", [ authJwt.isAdmin ], users.findOne);
    // Update a user with id
    router.put("/:id", [ authJwt.verifyToken ], users.update);
    // Delete a user with id
    router.delete("/:id",[ authJwt.verifyToken], users.delete);
    // Delete all user
    router.delete("/", [ authJwt.verifyToken], users.deleteAll);


    router.get("all", users.allAccess);
    router.get(
      "/user",
      [authJwt.verifyToken],
      users.userBoard
    );
  
    router.get(
      "/admin",
      [authJwt.verifyToken, authJwt.isAdmin],
      users.adminBoard
    );

    app.use('/api/users', router);
    app.use('/api/test', router)
    };