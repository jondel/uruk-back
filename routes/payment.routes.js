const { authJwt, dao, payment } = require("../middleware");
const controller = require("../controllers/payment.controller.js");

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
      app.post(
        "/api/categorie/payment/create",
        [ 
            authJwt.verifyToken,
            dao.isDaoOwner,
            payment.hasCategorie ],
        controller.create
      );
    
      app.get(
        "/api/categorie/payment",
        [
            authJwt.verifyToken,
            dao.isDaoOwner,
            payment.hasCategorie ],
        controller.findAll
      );

      app.get(
        "/api/categorie/payment/:id",
        [
            authJwt.verifyToken,
            dao.isDaoOwner,
            payment.hasCategorie ],
        controller.findOne
      );
}