const { authJwt, dao } = require("../middleware");
const controller = require("../controllers/collection.controller.js");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // TODO : middleware to check if dao has already a collection set up
  app.get(
    "/api/collection",
    [ 
        authJwt.verifyToken,
        dao.hasDaoOwnerRole
    ],
    controller.findOne
  );
}