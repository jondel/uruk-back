const { authJwt, dao } = require("../middleware");
const controller = require("../controllers/dao.controller.js");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    "/api/dao/create",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole],
    controller.create
  );

  app.get(
    "/api/dao",
    [
      authJwt.verifyToken,
      authJwt.isAdmin],
    controller.findAll
  )

  app.get(
    "/api/dao/:id",
    [
      authJwt.verifyToken,
      dao.isDaoOwner
    ],
    controller.findOne
  )

  app.post(
    '/api/delete/dao',
    [
      authJwt.verifyToken,
      dao.isDaoOwner
    ],
    controller.delete
  )

  app.post(
    '/api/update/dao',
    [
      authJwt.verifyToken,
      dao.isDaoOwner
    ],
    controller.update
  )
};