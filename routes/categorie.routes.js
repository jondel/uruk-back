const { authJwt, dao } = require("../middleware");
const controller = require("../controllers/categorie.controller.js");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    "/api/categorie/create",
    [ 
        authJwt.verifyToken,
        dao.isDaoOwner ],
    controller.create
  );

  app.post(
    "/api/delete/categorie",
    [
      authJwt.verifyToken,
      dao.isDaoOwner
    ],
    controller.delete
  )

  app.get(
    "/api/all/categorie",
    [
        authJwt.verifyToken,
        dao.hasDaoOwnerRole ],
    controller.findAll
  );

  app.get(
    "/api/admin/categorie",
    [
        authJwt.verifyToken,
        authJwt.isAdmin ],
    controller.findAll
  )
};