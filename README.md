# DAOTOOL BACKEND

### HOW USE THIS BACKEND

### THIS BELOW INSTRUCTIONS IS FOR LINUX OS !!!


**1.Essential tools to make this repo work locally**

<br>
1.node.js link : https://nodejs.org/en/ <br>
2.VS Code (or other editor) <br>
3.MySql link : https://www.mysql.com/fr/ <br>
4.Git link : https://git-scm.com/ <br>
5. Postan link : https://www.postman.com/ <br>

<br>

**2.Git**
<br>

Download this repo : 

```bash
git clone https://gitlab.com/jondel/daotool.git
```

<br>

**3.Create database**
<br>


In the terminal, first type

```bash
sudo mysql
```

Then create the database for the project and leave the DBMS

```bash
CREATE DATABASE dao;    <= this name is an example
EXIT;
```

<br>

**4.Install dependencies**
<br>

Go the folder : 

```bash
cd BACKEND
```

And run the following command :

```bash
npm Install
```
<br>

**5. Initialize the configuration**
<br>

In your project folder, make a copy of ``.env.example``

```bash
cp .env.example .env
```

Now open the file ``.env`` and make sure the following variables a set as follow :

```bash
USER=<your mySql username> 
PASSWORD=<your mysSql password>
DB_NAME=<your database name>
```
<br>

**6. Run the project**
<br>

Run this following command to launch your server locally : 

```bash
node server.js
```

<br>

### HOW USE THIS PROJECT FOR MAKING REQUEST

**1.Endpoint**

Follow these below routes : 
<br>

SignIn and SignUp routes : 

```bash
/api/auth/signup Method: POST
/api/auth/signin Method: POST
```
<br>

Dao routes :
<br>
```bash
/api/dao/create Method: POST
/api/dao/:id    Method: GET
```

**To be continued...**





