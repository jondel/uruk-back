const db = require("../models");
const Dao = db.dao;
const User = db.user;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    Dao.create({
        name: req.body.daoName
    })
        .then(dao => {
            User.findOne({
                where: req.body.userId
            }).then(user => {
                user.setDaos(dao).then(() => {
                    res.send({
                        message: "DAO was create successfully!",
                        dao: dao
                    });
                });
            });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
}

exports.findAll = (req, res) => {
    const name = req.query.name;
    var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;
    Dao.findAll({ where: condition }).then(dao => {
        res.status(200).send(dao);
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving daos."
            })
        })
}

exports.findOne = (req, res) => {
    const id = req.params.id;
    Dao.findByPk(id).then(dao => {
        if (!dao) {
            return res.status(404).send({ message: "Dao Not found." });
        }
        var categories = [];
        dao.getCategories().then(cat => {
            for (let i = 0; i < cat.length; i++) {
                categories.push(cat[i].name);
            }
            res.status(200).send({
                daoId: dao.id,
                daoName: dao.name,
                categories: categories
            });
        })
    })
}

exports.delete = async (req, res) => {
    const userId = req.body.userId
    const daoId = req.body.daoId;
    try {
        const user = await User.findByPk(userId);
        user.update({ 
            isActive: false,
            address: ""
         });
        const dao = await Dao.findByPk(daoId);
        dao.update({isDeleted : true});

        res.status(200).send({ message: 'DAO is deleted !'});

    } catch(err) {
        console.log(err);
        res.status(400).send({ message : 'A problem occur for deleting user'});
    }
    
}

exports.update = async (req, res) => {
    const daoId = req.body.daoId;

    try {
        const dao = await Dao.findByPk(daoId);
        dao.update({name : req.body.name });
        res.status(200).send({ message : "DAO name has been updated !!", name: req.body.name})

    } catch(err) {
        console.log(err);
        res.status(400).send({ message : 'A problem occur for deleting user'});
    }
}