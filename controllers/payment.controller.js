const { user } = require("../models");
const db = require("../models");
const Dao = db.dao;
const Categorie = db.categories;
const Payment = db.payments;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    Payment.create({
        name : req.body.name,
        address : req.body.address,
        amount : req.body.amount,
    })
    .then(payment => {
        Categorie.findOne({
            where : req.body.categorieId
        }).then(cat => {
            cat.setPayments(payment).then( () => {
                user.findOne({
                    where : req.body.userId
                }).then(user => {
                    res.status(200).send({ 
                        message: "Payments was create successfully!",
                        "payment" : payment,
                        "cat_payment" : cat,
                        "User" : user.name
                     })
                })
                
            })
        })
    })
    .catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.findAll = (req, res) => {
    Payment.findAll().then(payments => {
        res.status(200).send(payments);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving payments."
        })
    })
}

exports.findOne = (req, res) => {
    const id = req.params.id;
    Payment.findByIk(id)
        .then(payment => {
            if(!payment) return res.status(404).send({ message: "Payment Not found." });
            res.status(200).send(payment);
        })
        .catch(err => {
            res.status(500).send({
                message  : err.message || "Some error occurred while retrieving payment."
            })
        })
}