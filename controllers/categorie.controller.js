const { user } = require("../models");
const db = require("../models");
const Categorie = db.categories;
const User = db.user;
const Dao = db.dao;
const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
    try {
        const categorie = await Categorie.create({
            name: req.body.name,
            daoId: req.body.daoId
        })
        res.status(200).send({ message: "Categories was create successfully!", categorie: categorie });
    } catch(err) {
        res.status(500).send({ message: err.message });
    }
   
}

exports.delete = async (req, res) => {
    const dao = await Dao.findByPk(req.body.daoId);
    const categorie = await Categorie.findByPk(req.body.catId);
    try {
        const response = dao.removeCategories(categorie);
        res.status(200).send({ message: "Categorie deleted successfully" })

    } catch (err) {
        res.status(500).send({ message: err.message });
    }

}

exports.findAll = async (req, res) => {
    try {
        const categories = await Categorie.findAll({
            where: {
                daoId: req.query.daoId
            }
        })
        if(categories.length == 0) return res.status(404).send({ message: "No categories found" });
        res.status(200).send({
            categories: categories
        });
    } catch(err) {
        res.status(500).send({message: err.message || "Some error occurred while retrieving categories."})
    }
}

exports.findOne = async (req, res) => {
    const id = req.params.id;
    try {
        const categorie = await Categorie.findByPk(id);
        if (!categorie) return res.status(404).send({ message: "Categorie Not found." });
        res.status(200).send(categorie);
    } catch(err) {
        res.status(500).send({
            message: err.message || `Some error occurred while retrieving categorie ${id}`
        })
    }
}