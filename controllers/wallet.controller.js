const db = require("../models");
const Wallet = db.wallet;
const Dao = db.dao;
const Op = db.Sequelize.Op;
const axios = require('axios');

const LAMPORT = 1000000000;

const TokenService = require('../services/token.service.js');


exports.add = async (req, res) => {
    let wallet = await Wallet.create({
        name: req.body.name,
        address: req.body.address,
        nativeBalance: req.body.nativeBalance
    })

    let dao = await Dao.findOne({
        where: req.body.daoId
    })

    try {

        let newWallet = await TokenService.updateNativeBalance(req.body.address, req.body.daoId, wallet);

        dao.addWallets(wallet);
        res.status(200).send({
            message: "Wallet was added successfully!",
            wallet: newWallet
        });

    } catch (err) {
        res.status(400).send({
            message: err.message || "An error when fetching balance at the creation of wallet"
        })
    }

}

exports.findAllWallets = async (req, res) => {
    const id = req.query.daoId;
    try {
        const dao = await Dao.findByPk(id);
        const wallets = await dao.getWallets({ where: { daoId: id } });

        if (!wallets) {
            res.status(404).send({ message: "Wallets not found" })
        }
        wallets.forEach(async (wallet) => {
            try {
                await TokenService.updateNativeBalance(wallet.address, id, wallet);
            } catch (err) {
                console.log(err)
            }

        })
        res.status(200).send({
            wallets: wallets
        })
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
}

exports.findOne = (req, res) => {
    const id = req.params.id;
    Wallet.findByPk(id).then(wallet => {
        if (!wallet) {
            return res.status(404).send({ message: "Wallet Not found." });
        }
        res.status(200).send({
            wallet: wallet
        });
    })
}

exports.delete = (req, res) => {
    Wallet.findByPk(req.body.walletId).then(wallet => {
        Dao.findByPk(req.body.daoId).then(dao => {
            dao.removeWallets(wallet).then(response => {
                res.status(200).send({ message: "Wallet deleted successfuly" });
            }).catch(err => {
                res.send({ message: err.message });
            })
        })
    })
}

exports.walletToken = async (req, res) => {
    if (!req.body.address) {
        res.status(400).send({
            message: "No address provided"
        })
    } else {
        try {
            const options = {
                method: 'POST',
                url: 'https://rest-api.hellomoon.io/v0/token/balances-by-owner',
                headers: {
                  accept: 'application/json',
                  'content-type': 'application/json',
                  authorization: `Bearer ${process.env.HELLOMOON_API_KEY}`
                },
                data: {ownerAccount: `${req.body.address}`}
              };
            
            const { data } = await axios.request(options);
            
            // Format the token balance for the front
            const walletToken = await TokenService.getTokenData(data);

            res.status(200).send(walletToken);

        } catch (err) {
            console.log(err.message);
            res.status(400).send({
                message: err.message || "An error "
            })
        }
    }
}

exports.nftWallet = async (req, res) => {
    if(!req.body.address) {
        res.status(400).send({
            message: "No address provided"
        })
    } else {
        try {
            const options = {
                method: 'POST',
                url: 'https://rest-api.hellomoon.io/v0/token/balances-by-owner',
                headers: {
                  accept: 'application/json',
                  'content-type': 'application/json',
                  authorization: `Bearer ${process.env.HELLOMOON_API_KEY}`
                },
                data: {ownerAccount: `${req.body.address}`}
              };
            
            const { data } = await axios.request(options);

            const metadata = await TokenService.getNftMetadata(data);

            res.status(200).send(metadata);

        } catch (err) {
            console.log(err.message);
            res.status(400).send({
                message: err.message || "An error "
            })
        }
    }
}