const axios = require('axios');

//Et5d2EzhUtGt2AA8rm1Gqj2pxRCTvcUmYAGJ8g74iq5g

checkTypeOfTx = (tx) => {
    const type = tx.type.split('_');

    return type[0];
}

createDescription = async (tx) => {
    try {
        const type = checkTypeOfTx(tx);
        //console.log(tx);
        if (type === 'NFT') {
            const nftData = await getNftMetadata(tx.data.mint);
            const description = returnDescriptionIfNft(tx, nftData);

            return {
                description: description,
                nftData : nftData
            };
        }
        if (type === 'TOKEN') {
            const sourceMint = await getTokenMetadata(tx.data.sourceMint);
            const destinationMint = await getTokenMetadata(tx.data.destinationMint);
            const description = returnDescriptionIfToken(tx, sourceMint, destinationMint);

            return description;
        }
    } catch (err) {
        return err
    }
}

returnDescriptionIfNft = (tx, nftData) => {
    try {
        //console.log(nftData);
        //console.log(tx)
        if (tx.type === 'NFT_LISTING/DELISTING' && !tx.data.isListing) {
            let description = `The user ${tx.userAccount} delist ${nftData.data[0].nftMetadataJson?.name}`;
            return description;
        }
        if (tx.type === 'NFT_LISTING/DELISTING' && tx.data.isListing) {
            let description = `The user ${tx.userAccount} list ${nftData.data[0].nftMetadataJson?.name} for ${tx.data.price / 1000000000} SOL`;
            return description;
        }
        if (tx.type === 'NFT_SALE') {
            let description = `The user ${tx.userAccount} sale ${nftData.data[0].nftMetadataJson?.name} for ${tx.data.price} SOL`;
            return description;
        }
        if (tx.type === 'NFT_MINT') {
            let description = `The user ${tx.userAccount} mint ${nftData.data[0].nftMetadataJson?.name || nftData.data[0].nftMetadataJson.symbol} for ${tx.data.amount} SOL`;
            return description;
        }
    } catch (err) {
        return err
    }
}

returnDescriptionIfToken = (tx, sourceMint = NULL, destinationMint = NULL) => {
    try {
        //console.log(tx);
        let solAmount = 0;
        const LAMPORT = 1000000000;
        if (sourceMint || destinationMint != NULL) {
            if (sourceMint.mint === 'So11111111111111111111111111111111111111112' || destinationMint.mint === 'So11111111111111111111111111111111111111112') {
                solAmount += tx.data.sourceAmount / LAMPORT || tx.data.destinationAmount / LAMPORT;
                if (tx.type === 'TOKEN_SWAP') {
                    if (solAmount > 0) {
                        let description = `The user ${tx.userAccount} swap ${destinationMint.symbol} from ${sourceMint.symbol} for ${solAmount}`;

                        return description;
                    } else {
                        let description = `The user ${tx.userAccount} swap ${destinationMint.symbol} from ${sourceMint.symbol} for ${tx.data.destinationAmount}`;

                        return description;
                    }
                }
                if (tx.type === 'TOKEN_TRANSFER') {
                    let description = `The user ${tx.userAccount} send ${tx.data.amount} of ${sourceMint.symbol} to ${tx.data.destinationOwner}`;

                    return description;
                }
            } else {
                if (tx.type === 'TOKEN_SWAP') {
                    let description = `The user ${tx.userAccount} swap ${destinationMint.symbol} from ${sourceMint.symbol} for ${tx.data.sourceAmount / LAMPORT} ${sourceMint.symbol} and receive ${tx.data.destinationAmount} ${destinationMint.symbol}`;

                    return description;
                }
                if (tx.type === 'TOKEN_TRANSFER') {
                    let description = `The user ${tx.userAccount} send ${tx.data.amount} of ${sourceMint.symbol} to ${tx.data.destinationOwner}`;

                    return description;
                }
            }
        }
    } catch (err) {
        return err
    }
}

getNftMetadata = async (mint) => {
    const options = {
        method: 'POST',
        url: 'https://rest-api.hellomoon.io/v0/nft/mint_information',
        headers: {
            accept: 'application/json',
            'content-type': 'application/json',
            authorization: `Bearer ${process.env.HELLOMOON_API_KEY}`
        },
        data: { nftMint: `${mint}` }
    };

    try {
        const { data } = await axios.request(options);
        if (data) {
            return data;
        }
    } catch (err) {
        return err.message
    }
}

getTokenMetadata = async (mint) => {
    const axios = require('axios');

    const options = {
        method: 'POST',
        url: 'https://rest-api.hellomoon.io/v0/token/list',
        headers: {
            accept: 'application/json',
            'content-type': 'application/json',
            authorization: 'Bearer 87489b16-f923-4a34-81a7-49d59c52da93'
        },
        data: { mint: `${mint}` }
    };

    try {
        const { data } = await axios.request(options);
        if (data) {
            return data.data[0]
        }
    } catch (err) {
        return err.message
    }
}

const description = {
    createDescription
}

module.exports = description;